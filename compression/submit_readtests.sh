#!/bin/bash

dir="/apps/contrib/redis/testing_scripts/compression"
logfile=$dir/read_results.csv
readcode=$dir/readfile.py
echo "running on $HOSTNAME"
comp=$1
var=$3

filedir="/group_workspaces/jasmin/hiresgw/vol1/mj07/IO_testing_files/comp_test_"

testfile=$filedir$var
testfile+="_c"$comp
testfile+=".nc"


read_mode=$2

echo $chunksize $readcode $testfile $read_mode
$readcode $testfile $read_mode $comp $var >> $logfile

echo 'Finished test'
