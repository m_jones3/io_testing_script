#!/bin/bash

rundir="/apps/contrib/redis/testing_scripts/numpy_testing"
testfile=/group_workspaces/jasmin/hiresgw/vol1/mj07/IO_testing_files/test.bin
logfile=$rundir/read_results.csv
readcode=$rundir/readfile.py

if [ "$#" -eq 3 ]; then
    block_size=$3
    read_mode=$2

    echo $readcode $testfile $block_size $read_mode
    $readcode $testfile $read_mode $block_size >> $logfile
    fi

if [ "$#" = 4 ]; then
    rand_num=$4
    block_size=$3
    read_mode=$2

    echo $readcode $testfile $block_size $read_mode $randnum
    $readcode $testfile $read_mode $block_size $rand_num >> $logfile
    fi

echo 'Finished test'
