#!/usr/bin/env python2.7

import sys, os
import csv
from jug import TaskGenerator

@TaskGenerator
def run_task(task):
    testdir = '/apps/contrib/redis/testing_scripts/'+task[0]
    chunksize = task[1]
    try:
        comp = int(chunksize)
        bool_comp = True
    except ValueError:
        bool_comp = False
    readmode = task[2]
    sub_file = testdir+'/submit_readtests.sh'
    if len(task) == 4 and bool_comp: #sequential and hopping            
        var = task[3]
        os.system('%s %s %s %s' % (sub_file, comp, readmode, var))
    elif len(task) == 4: #sequential and hopping            
        readsize = task[3]
        os.system('%s %s %s %s' % (sub_file, chunksize, readmode, readsize))
    elif len(task) == 5: #random
        readsize = task[3]
        randnum = task[4]
        os.system('%s %s %s %s %s' % (sub_file, chunksize, readmode, readsize, randnum))
    elif len(task) == 3: # 2d and 4d
        os.system('%s %s %s' % (sub_file, chunksize, readmode))
    else:
        raise ValueError('Task list error, see examples')

    return 0

conc_tasks = int(sys.argv[1])
tasks_file = sys.argv[2]

with open(tasks_file, 'rb') as f:
    reader = csv.reader(f)
    tasks = list(reader)


map(run_task,tasks)
