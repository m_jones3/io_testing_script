#!/usr/bin/env python2.7
from mpi4py import MPI
from socket import gethostname
from os import system
from sys import argv

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
if rank == 0:
    print 'parent rank',rank,'running on host', gethostname()
    system('jug execute %s/submission_jug.py %s %s' %(argv[1],argv[2],argv[3]))
else:
    print 'parent rank',rank,'running on host', gethostname()
    system('jug execute %s/submission_jug.py %s %s' %(argv[1],argv[2],argv[3]))
